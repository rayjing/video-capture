﻿using System;
using System.Drawing;
using System.Windows.Media;
namespace YK
{
    public enum ECameraAngle
    {
        A0,
        A90,
        A180,
        A270
    }
    public interface IVideo
    {
        /// <summary>
        /// 摄像头初始化
        /// </summary>
        /// <param name="index">摄像头序号</param>
        /// <param name="angle">摄像头角度</param>
        /// <param name="frameWidth">视频输出的宽度</param>
        /// <param name="frameHeight">视频输出的高度</param>
        /// <returns></returns>
        void Init(int index, int frameWidth = 640, int frameHeight = 480, ECameraAngle angle = ECameraAngle.A0);
        /// <summary>
        /// 打开摄像头
        /// </summary>
        void Play();
        /// <summary>
        /// 关闭摄像头
        /// </summary>
        void Stop();
        event Action<ImageSource> ImageSourceChanged;
        /// <summary>
        /// 获取当前帧
        /// </summary>
        Bitmap GetCurrentFrame();
    }
}
