﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Security;
namespace YK
{
    [ComImport, SuppressUnmanagedCodeSecurity, Guid("56a86891-0ad4-11ce-b03a-0020af0ba770"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IPin
    {
    }
    [ComImport, SuppressUnmanagedCodeSecurity, Guid("56a86895-0ad4-11ce-b03a-0020af0ba770"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IBaseFilter
    {
    }
    public enum FilterState
    {
        Stopped,
        Paused,
        Running
    }

    [ComImport, SuppressUnmanagedCodeSecurity, Guid("56a868b1-0ad4-11ce-b03a-0020af0ba770"), InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IMediaControl
    {
        [PreserveSig]
        int Run();

        [PreserveSig]
        int Pause();

        [PreserveSig]
        int Stop();
        [PreserveSig]
        int GetState([In] int msTimeout, [Out] out FilterState pfs);
    }
    [ComImport, SuppressUnmanagedCodeSecurity, Guid("56a8689f-0ad4-11ce-b03a-0020af0ba770"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IFilterGraph
    {
        [PreserveSig]
        int AddFilter([In] IBaseFilter pFilter, [In, MarshalAs(UnmanagedType.LPWStr)] string pName);
        [PreserveSig]
        int RemoveFilter([In] IBaseFilter pFilter);
        [PreserveSig]
        int EnumFilters([Out] out object ppEnum);
        [PreserveSig]
        int FindFilterByName([In, MarshalAs(UnmanagedType.LPWStr)] string pName, [Out] out IBaseFilter ppFilter);
        [PreserveSig]
        int ConnectDirect([In] IPin ppinOut, [In] IPin ppinIn, [In, MarshalAs(UnmanagedType.LPStruct)] AMMediaType pmt);
        [PreserveSig]
        [Obsolete("This method is obsolete; use the IFilterGraph2.ReconnectEx method instead.")]
        int Reconnect([In] IPin ppin);
        [PreserveSig]
        int Disconnect([In] IPin ppin);
        [PreserveSig]
        int SetDefaultSyncSource();
    }
    [ComImport, SuppressUnmanagedCodeSecurity, Guid("56a868a9-0ad4-11ce-b03a-0020af0ba770"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IGraphBuilder : IFilterGraph
    {
        #region IFilterGraph Methods
        [PreserveSig]
        new int AddFilter([In] IBaseFilter pFilter, [In, MarshalAs(UnmanagedType.LPWStr)] string pName);
        [PreserveSig]
        new int RemoveFilter([In] IBaseFilter pFilter);
        [PreserveSig]
        new int EnumFilters([Out] out object ppEnum);
        [PreserveSig]
        new int FindFilterByName([In, MarshalAs(UnmanagedType.LPWStr)] string pName, [Out] out IBaseFilter ppFilter);
        [PreserveSig]
        new int ConnectDirect([In] IPin ppinOut, [In] IPin ppinIn, [In, MarshalAs(UnmanagedType.LPStruct)] AMMediaType pmt);
        [PreserveSig]
        new int Reconnect([In] IPin ppin);
        [PreserveSig]
        new int Disconnect([In] IPin ppin);
        [PreserveSig]
        new int SetDefaultSyncSource();
        #endregion
        [PreserveSig]
        int Connect([In] IPin ppinOut, [In] IPin ppinIn);
        [PreserveSig]
        int Render([In] IPin ppinOut);
        [PreserveSig]
        int RenderFile([In, MarshalAs(UnmanagedType.LPWStr)] string lpcwstrFile, [In, MarshalAs(UnmanagedType.LPWStr)] string lpcwstrPlayList);
        [PreserveSig]
        int AddSourceFilter([In, MarshalAs(UnmanagedType.LPWStr)] string lpcwstrFileName, [In, MarshalAs(UnmanagedType.LPWStr)] string lpcwstrFilterName, [Out] out IBaseFilter ppFilter);
        [PreserveSig]
        int SetLogFile(IntPtr hFile); // DWORD_PTR
        [PreserveSig]
        int Abort();
        [PreserveSig]
        int ShouldOperationContinue();
    }
    [ComImport, SuppressUnmanagedCodeSecurity, Guid("36b73882-c2c8-11cf-8b46-00805f6cef60"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IGraphBuilder2 : IGraphBuilder
    {
        #region IFilterGraph Methods
        [PreserveSig]
        new int AddFilter([In] IBaseFilter pFilter, [In, MarshalAs(UnmanagedType.LPWStr)] string pName);
        [PreserveSig]
        new int RemoveFilter([In] IBaseFilter pFilter);
        [PreserveSig]
        new int EnumFilters([Out] out object ppEnum);
        [PreserveSig]
        new int FindFilterByName([In, MarshalAs(UnmanagedType.LPWStr)] string pName, [Out] out IBaseFilter ppFilter);
        [PreserveSig]
        new int ConnectDirect([In] IPin ppinOut, [In] IPin ppinIn, [In, MarshalAs(UnmanagedType.LPStruct)] AMMediaType pmt);
        [PreserveSig]
        new int Reconnect([In] IPin ppin);
        [PreserveSig]
        new int Disconnect([In] IPin ppin);
        [PreserveSig]
        new int SetDefaultSyncSource();
        #endregion
        #region IGraphBuilder Method
        [PreserveSig]
        new int Connect([In] IPin ppinOut, [In] IPin ppinIn);
        [PreserveSig]
        new int Render([In] IPin ppinOut);
        [PreserveSig]
        new int RenderFile([In, MarshalAs(UnmanagedType.LPWStr)] string lpcwstrFile, [In, MarshalAs(UnmanagedType.LPWStr)] string lpcwstrPlayList);
        [PreserveSig]
        new int AddSourceFilter([In, MarshalAs(UnmanagedType.LPWStr)] string lpcwstrFileName, [In, MarshalAs(UnmanagedType.LPWStr)] string lpcwstrFilterName, [Out] out IBaseFilter ppFilter);
        [PreserveSig]
        new int SetLogFile(IntPtr hFile); // DWORD_PTR
        [PreserveSig]
        new int Abort();
        [PreserveSig]
        new int ShouldOperationContinue();
        #endregion
        [PreserveSig]
        int AddSourceFilterForMoniker([In] IMoniker pMoniker, [In] IBindCtx pCtx, [In, MarshalAs(UnmanagedType.LPWStr)] string lpcwstrFilterName, [Out] out IBaseFilter ppFilter);
    }
    [ComImport, SuppressUnmanagedCodeSecurity, Guid("93E5A4E0-2D50-11d2-ABFA-00A0C9C6E38D"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface ICaptureGraphBuilder2
    {
        [PreserveSig]
        int SetFiltergraph([In] IGraphBuilder pfg);
        [PreserveSig]
        int GetFiltergraph([Out] out IGraphBuilder ppfg);
        [PreserveSig]
        int SetOutputFileName([In, MarshalAs(UnmanagedType.LPStruct)] Guid pType, [In, MarshalAs(UnmanagedType.LPWStr)] string lpstrFile, [Out] out IBaseFilter ppbf, [Out] out object ppSink);
        [PreserveSig]
        int FindInterface([In, MarshalAs(UnmanagedType.LPStruct)] Guid pCategory, [In, MarshalAs(UnmanagedType.LPStruct)] Guid pType, [In] IBaseFilter pbf, [In, MarshalAs(UnmanagedType.LPStruct)] Guid riid, [Out, MarshalAs(UnmanagedType.IUnknown)] out object ppint);
        [PreserveSig]
        int RenderStream([In, MarshalAs(UnmanagedType.LPStruct)] Guid PinCategory, [In, MarshalAs(UnmanagedType.LPStruct)] Guid MediaType, [In, MarshalAs(UnmanagedType.IUnknown)] object pSource, [In] IBaseFilter pfCompressor, [In] IBaseFilter pfRenderer);
    }

    [ComImport, SuppressUnmanagedCodeSecurity, Guid("6B652FFF-11FE-4fce-92AD-0266B5D7C78F"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface ISampleGrabber
    {
        [PreserveSig]
        int SetOneShot([In, MarshalAs(UnmanagedType.Bool)] bool OneShot);
        [PreserveSig]
        int SetMediaType([In, MarshalAs(UnmanagedType.LPStruct)] AMMediaType pmt);
        [PreserveSig]
        int GetConnectedMediaType([Out, MarshalAs(UnmanagedType.LPStruct)] AMMediaType pmt);
        [PreserveSig]
        int SetBufferSamples([In, MarshalAs(UnmanagedType.Bool)] bool BufferThem);
        [PreserveSig]
        int GetCurrentBuffer(ref int pBufferSize, IntPtr pBuffer);
        [PreserveSig]
        int GetCurrentSample(out IntPtr ppSample);
        [PreserveSig]
        int SetCallback(ISampleGrabberCB pCallback, int WhichMethodToCallback);
    }
    [ComImport, SuppressUnmanagedCodeSecurity, Guid("0579154A-2B53-4994-B0D0-E773148EFF85"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface ISampleGrabberCB
    {
        [PreserveSig]
        int SampleCB(double SampleTime, IntPtr pSample);
        [PreserveSig]
        int BufferCB(double SampleTime, IntPtr pBuffer, int BufferLen);
    }
    [ComImport, SuppressUnmanagedCodeSecurity, Guid("29840822-5B84-11D0-BD3B-00A0C911CE86"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface ICreateDevEnum
    {
        [PreserveSig]
        int CreateClassEnumerator([In, MarshalAs(UnmanagedType.LPStruct)] Guid pType, [Out] out IEnumMoniker ppEnumMoniker, [In] int dwFlags);
    }
    [ComImport, SuppressUnmanagedCodeSecurity, Guid("C6E13340-30AC-11d0-A18C-00A0C9118956"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IAMStreamConfig
    {
        [PreserveSig]
        int SetFormat([In, MarshalAs(UnmanagedType.LPStruct)] AMMediaType pmt);
        [PreserveSig]
        int GetFormat([Out] out AMMediaType pmt);
    }
    [StructLayout(LayoutKind.Sequential)]
    public class AMMediaType
    {
        public Guid majorType;
        public Guid subType;
        [MarshalAs(UnmanagedType.Bool)] public bool fixedSizeSamples;
        [MarshalAs(UnmanagedType.Bool)] public bool temporalCompression;
        public int sampleSize;
        public Guid formatType;
        public IntPtr unkPtr; // IUnknown Pointer
        public int formatSize;
        public IntPtr formatPtr; // Pointer to a buff determined by formatType 指向VideoInfo
    }
    [ComImport, Guid("e436ebb8-524f-11ce-9f53-0020af0ba770")]
    public class FilterGraphNoThread
    {
    }
    [ComImport, Guid("BF87B6E1-8C27-11d0-B3F0-00AA003761C5")]
    public class CaptureGraphBuilder2
    {
    }
    [StructLayout(LayoutKind.Sequential)]
    public class DsRect
    {
        public int left;
        public int top;
        public int right;
        public int bottom;
    }
    [StructLayout(LayoutKind.Sequential)]
    public class VideoInfoHeader
    {
        public DsRect SrcRect;
        public DsRect TargetRect;
        public int BitRate;
        public int BitErrorRate;
        public long AvgTimePerFrame;
        public BitmapInfoHeader BmiHeader;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 2)]
    public class BitmapInfoHeader
    {
        public int Size;//本结构大小40字节
        public int Width;
        public int Height;
        public short Planes;
        public short BitCount;
        public int Compression;
        public int ImageSize;
        public int XPelsPerMeter;
        public int YPelsPerMeter;
        public int ClrUsed;
        public int ClrImportant;
    }
    [ComImport, Guid("C1F400A0-3F08-11d3-9F0B-006008039E37")]
    public class SampleGrabber
    {
    }
    [ComImport, Guid("62BE5D10-60EB-11d0-BD3B-00A0C911CE86")]
    public class CreateDevEnum
    {
    }
}
