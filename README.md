# VideoCapture

#### 介绍
Wpf使用的摄像头视频捕捉类

DirectShowLib-2005.dll功能很强大，但：

1. 我只要显示摄像头视频，偶尔拍张照；
1. 不想拖个dll；
1. 洁癖

所以把能删的都删了，只留下我想要的，编译后才17K。需要的时候可以直接嵌入到项目中。

#### 使用
```
    public interface IVideo
    {
        /// <summary>
        /// 摄像头初始化
        /// </summary>
        /// <param name="index">摄像头序号</param>
        /// <param name="angle">摄像头角度</param>
        /// <param name="frameWidth">视频输出的宽度</param>
        /// <param name="frameHeight">视频输出的高度</param>
        /// <returns></returns>
        void Init(int index, int frameWidth = 640, int frameHeight = 480, ECameraAngle angle = ECameraAngle.A0);
        /// <summary>
        /// 打开摄像头
        /// </summary>
        void Play();
        /// <summary>
        /// 关闭摄像头
        /// </summary>
        void Stop();
        event Action<ImageSource> ImageSourceChanged;
        /// <summary>
        /// 获取当前帧
        /// </summary>
        Bitmap GetCurrentFrame();
    }
```