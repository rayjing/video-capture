﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Media;

using YK;

namespace WpfAppVideoCapture
{
    public partial class MainWindow : Window
    {
        IVideo VC = new VideoCapture();
        VMMain VMMain = new VMMain();

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = VMMain;
            //VMMain.Images = VC._BmpSource;
            VC.Init(1, 640, 480, ECameraAngle.A0);
            VC.ImageSourceChanged += VC_ImageSourceChanged;
            VC.Play();

        }


        private void VC_ImageSourceChanged(ImageSource imageSouce) => VMMain.Images = imageSouce;

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            VC.Play();
        }


        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            using var bitmap = VC.GetCurrentFrame();
            //人脸识别...
            bitmap.Save($@"d:\test\{DateTime.Now.Ticks}.bmp", System.Drawing.Imaging.ImageFormat.Bmp);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            VC.Stop();
        }

    }

    public class VMMain : VMBase
    {
        public ImageSource Images
        {
            get => G<ImageSource>();
            set => S(value);

        }
    }
    public class VMBase : INotifyPropertyChanged
    {
        private readonly Dictionary<string, object> CDField = new Dictionary<string, object>();

        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged([CallerMemberName] string propertyName = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        public T G<T>([CallerMemberName] string propertyName = "")
        {
            if (!CDField.TryGetValue(propertyName, out object _propertyValue))
            {
                _propertyValue = default(T);
                CDField.Add(propertyName, _propertyValue);
            }
            return (T)_propertyValue;
        }
        public void S(object value, [CallerMemberName] string propertyName = "")
        {
            if (!CDField.ContainsKey(propertyName) || CDField[propertyName] != (object)value)
            {
                CDField[propertyName] = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
