﻿using System.Windows.Media;

namespace WpfAppVideoCapture1
{
    public class VMMain : VMBase
    {
        public ImageSource Images
        {
            get => G<ImageSource>();
            set => S(value);

        }
    }
}
