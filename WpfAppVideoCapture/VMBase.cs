﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace WpfAppVideoCapture1
{
    public class VMBase : INotifyPropertyChanged
    {
        private readonly Dictionary<string, object> CDField = new Dictionary<string, object>();

        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged([CallerMemberName] string propertyName = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        public T G<T>([CallerMemberName] string propertyName = "")
        {
            if (!CDField.TryGetValue(propertyName, out object _propertyValue))
            {
                _propertyValue = default(T);
                CDField.Add(propertyName, _propertyValue);
            }
            return (T)_propertyValue;
        }
        public void S(object value, [CallerMemberName] string propertyName = "")
        {
            if (!CDField.ContainsKey(propertyName) || CDField[propertyName] != (object)value)
            {
                CDField[propertyName] = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
